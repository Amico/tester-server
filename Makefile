#Makefile per progetto Amico concentratore.
#Inserire i file *.o come contenuto della var OBJECTS

OBJECTS = 	src/main.o \
			src/canbus/can.o \
			src/canbus/hilevel_can.o
#nome eseguibile
PROG_NAME    = TesterServer

#compilatore
COPT=-Wall -g
INC_PATH=-Iinclude
#SYSROOT=--sysroot=/home/devel/yocto/fsl-community-bsp/build/tmp/sysroots/tqimx6s/
#CROSS_COMPILE=/opt/poky/1.7.1/sysroots/i686-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-
CFLAGS=-march=armv7-a -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a9 $(SYSROOT) $(INC_PATH) $(COPT) # $(LIBS) $(INC_PATH) $(DEBUG_OPT)
CC=$(CROSS_COMPILE)gcc

LIBS=-lpthread  

all: progs

progs:	$(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(PROG_NAME) $(LIBS)
	@echo "\033[0;32m"
	@echo "Compilazione ARM completata!"
	@echo "\033[0m"


call: clean progs

clean:
	@-rm $(PROG_NAME) $(OBJECTS)
	@echo "Clean completata!"

.PHONY : all clean install call

