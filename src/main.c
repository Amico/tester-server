/*
 * main.c
 *
 *  Created on: 29/gen/2015
 *      Author: Luigi Scagnet - Egicon s.r.l.
 */

//System include
#include <sys/ioctl.h>
#include <net/if.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <signal.h>
//Project include
#include <define.h>
#include <can.h>
#include <hilevel_can.h>

struct tU8flags system_flags;
unsigned char send_ok = 0;
int sockfd , connfd, retval;

static void sigInt(int sig, siginfo_t *siginfo, void *context) {
	printf("User interrupt. Close socket..\n");
	LOOP = FALSE;
}


void* Tcan(void *parent_PID)
{
	can0_init_thread();
	while(TRUE){
		if(send_ok){
			stateMachineManager();
		}
		usleep(2000);
	}
	pthread_exit(NULL);
}


int main(int argc, char **argv)
{
	struct sigaction sig_int;
	/*CAN VAR */
	pthread_t can_thread;

	/*ETH VAR */
	//int sockfd , connfd, retval;
	struct sockaddr_in serv_addr;
	char buff[100];

	LOOP = TRUE;

	/**********************************************************/
	/* SEGNALI      *******************************************/
	/**********************************************************/
	debug("Creazione handler SIGINT\n");
	memset(&sig_int, 0, sizeof(sig_int));
	sig_int.sa_sigaction = &sigInt;
	sig_int.sa_flags = SA_SIGINFO;
	if (sigaction(SIGINT, &sig_int, NULL) != 0) {
		error("sig_int ");
	}

	/**********************************************************/
	/* CAN INIT      *********************************************/
	/**********************************************************/
	pthread_create( &can_thread, NULL, &Tcan, NULL );

	/**********************************************************/
	/* ETH INIT      **********************************************/
	/**********************************************************/
	sockfd  = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd  < 0){
		perror("ERROR: socket ");
		close(sockfd);
		exit(-1);
	}

	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
	    error("setsockopt(SO_REUSEADDR) failed");

	memset(&serv_addr, 0, sizeof(serv_addr));
	memset(buff, 0, sizeof(buff));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(5000);

	retval = bind(sockfd , (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if(retval < 0)
	{
		perror( "ERROR: bind  ");
		close(sockfd);
		exit(-1);
	}

	retval = listen(sockfd , 1);
	if(retval < 0)
	{
		perror("ERROR: listen ");
		close(sockfd);
		exit(-1);
	}

	while(LOOP)
	{
		connfd = accept(sockfd , (struct sockaddr*)NULL, NULL);
		if(connfd < 0)
		{
			perror("ERROR: accept ");
			exit(EXIT_FAILURE);
			close(sockfd);
		}

		retval = read( connfd, &buff[0], sizeof(buff) );
		if(retval < 0)
		{
			perror("ERROR: read ");
			exit(EXIT_FAILURE);
			close(sockfd);
		}

		//debug("Command: %s\n", buff );
		switch( buff[0] )
		{
		case '1':
			//return state of downloader
			sprintf(buff, "%02d", getDownloaderState()  );
			break;

		case '2':
			//receive file name
			if( setFileName(  &buff[1] ) == 0 )
				sprintf(buff, "OK");
			else
				sprintf(buff, "KO");
			break;

		case '3':
			//start downloader
			setDownloaderStateToInit();
			send_ok = 1;
			sprintf(buff, "OK");
			break;

		case '4':
			//start downloader
			sprintf(buff, "%d", getReadedBytePercentage());
			break;

		default:
			//comando sconosciuto
			break;
		}
		write(connfd, buff, strlen(buff));
		close(connfd);
		sleep(1);
	}
	debug("End\n");
	close(sockfd);
	exit(EXIT_SUCCESS);

	return 0;
}
