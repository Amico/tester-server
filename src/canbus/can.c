/*
 * can.c
 *
 *  Created on: 29/gen/2015
 *      Author: Luigi Scagnet - Egicon s.r.l.
 */
//System include
#include <string.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <linux/can.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>
//Project include
#include <define.h>
#include <can.h>
#include <hilevel_can.h>

#define MAIN_THREAD 	0
#define CHILD_THREAD 	1
#define MAX_THREADS 	CHILD_THREAD + 1

static int	socket_fdl [MAX_THREADS];
struct ifreq ifr;
struct sockaddr_can addr;

/*
 * Function Name: *canListener
 * Input:
 * Outup:
 */
void canRead( struct can_frame *frame ) {
	int nbytes;
	if ((nbytes = read(socket_fdl[CHILD_THREAD], frame, sizeof(struct can_frame))) < 0) {
		perror("canRead");
	}
}
/*
 * Function Name: getCanSocketStatus
 * Input:
 * Outup:
 */
int getCanSocketStatus(void)
{
	int error = 0;
	socklen_t len = sizeof (error);
	return getsockopt (socket_fdl[CHILD_THREAD], SOL_SOCKET, SO_ERROR, &error, &len );
}

/*
 * Function Name: canWrite
 * Input:
 * Outup:
 */
int canWrite( struct can_frame frame )
{

	write(socket_fdl[CHILD_THREAD], &frame, sizeof(frame));
	return 1;
}

/*
 * Function Name: can0_init
 * Input:
 * Outup:
 */
void can0_init_thread(void)
{
	static struct can_filter rfilter;

	debug( "Apertura socket can\n");
	if ( ( socket_fdl[CHILD_THREAD]  =  socket( PF_CAN,  SOCK_RAW, CAN_RAW)) < 0) {
		perror("socket");
	}

	debug("ioctl periferica can\n");
	strncpy(ifr.ifr_name, "can0", sizeof(ifr.ifr_name));
	ioctl( socket_fdl[CHILD_THREAD], SIOCGIFINDEX, &ifr);

	debug("bind periferica can\n");
	addr.can_family = PF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;
	bind(socket_fdl[CHILD_THREAD],  (struct sockaddr *) &addr, sizeof(addr));

	rfilter.can_id = 0x5AD;
	rfilter.can_mask = CAN_SFF_MASK;
	setsockopt( socket_fdl[CHILD_THREAD], SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(struct can_filter) );


}

/*
 * Function Name: can0_init
 * Input:
 * Outup:
 */
void can0_init_main(void)
{
	static struct can_filter rfilter;

	debug( "Apertura socket can\n");
	if ( ( socket_fdl[MAIN_THREAD]  =  socket( PF_CAN,  SOCK_RAW, CAN_RAW)) < 0) {
		perror("socket");
	}

	debug("ioctl periferica can\n");
	strncpy(ifr.ifr_name, "can0", sizeof(ifr.ifr_name));
	ioctl( socket_fdl[MAIN_THREAD], SIOCGIFINDEX, &ifr);

	debug("bind periferica can\n");
	addr.can_family = PF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;
	bind(socket_fdl[MAIN_THREAD],  (struct sockaddr *) &addr, sizeof(addr));

	rfilter.can_id = 0x5AD;
	rfilter.can_mask = CAN_SFF_MASK;
	setsockopt( socket_fdl[CHILD_THREAD], SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(struct can_filter) );

}

