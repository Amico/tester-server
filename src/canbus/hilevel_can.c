/*
 * hilevel_can.c
 *
 *  Created on: 15/giu/2015
 *      Author: Luigi Scagnet
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <linux/can.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/select.h>
#include <signal.h>
//Project include
#include <hilevel_can.h>
#include <can.h>

static char file_name[100];
static downloader_sm state = wait_new_cmd;
static tU8 last_command_sent = 0;
static tU8 buffer[ROWS * COLS];
static tU16 mf_sent_byte = 0;
static tU16 mf_to_send_byte = 0;
static tU8 time_btwn_packs = 0x00;
static tU8 mf_max_data_to_send = 0x00;
static tU8 frame_counter = 0;
static tU8 sent_packet = 0;
static tU8 packet_at_once = 0xff;
static tU32 readed_byte = 0x00000000;
static tU32 file_size = 0x00000000;
static tU16 ck = 0x0000;

pthread_mutex_t mutex_wait_ack;
static boolean wait_ack = FALSE;

pthread_mutex_t mutex_wait_fc;
static boolean wait_fc = FALSE;

pthread_mutex_t mutex_mf_in_progress;
static boolean mf_in_progress = FALSE;


pthread_mutex_t mutex_timeout_expired;
static boolean timeout_expired = TRUE;
pthread_t timeout_thread = NULL;
static tU8 retry = 0;

typedef struct str_thdata
{
	tU16 mseconds;
	char message[40];
} thdata;


/*
 *
 */
typedef enum {
	send_first_frame,
	send_other_frames,

} multiframe_sm;
static multiframe_sm mf_status = send_first_frame;


/*
 * Function Name: setTimeOutMsExpired
 * Input:
 * Outup:
 */
static void setTimeOutMsExpired( boolean val )
{
	pthread_mutex_lock( &mutex_timeout_expired );
	timeout_expired = val;
	pthread_mutex_unlock( &mutex_timeout_expired );
}

/*
 * Function Name: getTimeOutExpired
 * Input:
 * Outup:
 */
static boolean getTimeOutExpired( void )
{
	return timeout_expired;
}

/*
 * Function Name: setTimeOutMs
 * Input:
 * Outup:
 */
static void *timeOutThread( void *arg )
{
	thdata *data;
	int s;
	data = (thdata *) arg;

	debug("0thread arg = %d\n", data->mseconds );

	s = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	if (s != 0)
		debug("pthread_setcancelstate\n");

	if( data->mseconds >= 1000 ){
		debug("sleep %d \n", (data->mseconds/1000) );
		sleep( data->mseconds/1000 );
	} else {
		debug("usleep %d \n", data->mseconds*1000 );
		usleep( data->mseconds*1000 );
	}
	debug("setTimeOutMsExpired\n" );
	setTimeOutMsExpired(TRUE);
	pthread_exit(EXIT_SUCCESS);
}

/*
 * Function Name: setTimeOutMs
 * Input:
 * Outup:
 */
static void setTimeOutMs( tU16 mseconds )
{
	thdata data1;
	data1.mseconds = mseconds;
	setTimeOutMsExpired(FALSE);
	pthread_create( &timeout_thread, NULL, timeOutThread, &data1 );
}

/*
 * Function Name: setWaitAck
 * Input:
 * Outup:
 */
static void setWaitAck( boolean val )
{
	pthread_mutex_lock( &mutex_wait_ack );
	wait_ack = val;
	pthread_mutex_unlock( &mutex_wait_ack );
}

/*
 * Function Name: getWaitAck
 * Input:
 * Outup:
 */
static boolean getWaitAck( void )
{
	return wait_ack;
}

/*
 * Function Name: setWaitFc
 * Input:
 * Outup:
 */
static void setWaitFc( boolean val )
{
	pthread_mutex_lock( &mutex_wait_fc );
	wait_fc = val;
	pthread_mutex_unlock( &mutex_wait_fc );
}

/*
 * Function Name: getWaitFc
 * Input:
 * Outup:
 */
static boolean getWaitFc( void )
{
	return wait_fc;
}

/*
 * Function Name: setWaitFc
 * Input:
 * Outup:
 */
static void setMfInProgress( boolean val )
{
	pthread_mutex_lock( &mutex_mf_in_progress );
	mf_in_progress = val;
	pthread_mutex_unlock( &mutex_mf_in_progress );
}

/*
 * Function Name: getWaitFc
 * Input:
 * Outup:
 */
static boolean getMfInProgress( void )
{
	return mf_in_progress;
}

/*
 * Function Name: ackReader
 * Input:
 * Outup:
 */
static void *ackReader( void *n )
{
	struct can_frame frame;
	boolean loop = TRUE;
	void *res;

	while(loop)
	{
		if( getWaitAck() || getWaitFc() )
		{
			/*
			 * TODO: gestione select & timeout!
			 */
			canRead( &frame );
			if( frame.data[1] == ( last_command_sent + 0x40 ) )
			{
				/* ACK */
				if(data_transfer == state)
				{
					if( readed_byte >= file_size ) { // ( (ERASE_ADDR_END - ERASE_ADDR_START) + 1)  ){
						state = data_transfer_finish;
						data("FINISH! readed_byte %ld %lx %ld\n", readed_byte, readed_byte, file_size);
					}
				}
				else
					state ++;

				setWaitAck(FALSE);
				setMfInProgress(FALSE);
				retry = 0;

				if( timeout_thread != NULL){
					debug("Send pthread_cancel\n" );
					pthread_cancel(timeout_thread);
					pthread_join(timeout_thread, &res);
				}

				if(frame.data[1] == ( PREPARE_DATA_TRANSFER_CMD + 0x40 ) )
				{
					mf_max_data_to_send = frame.data[2];
				}
			}
			else if( frame.data[0] == 0x30 )
			{
				/* FlowControl */
				mf_status = send_other_frames;
				packet_at_once = frame.data[1];
				packet_at_once = (packet_at_once == 0) ? 3 : packet_at_once;
				time_btwn_packs = frame.data[2];
				sent_packet = 0;
				setWaitFc(FALSE);
			}
			else
			{
				error("Message Error state: %d\n", state);
				state = error_state;
			}
		} else {
			usleep(100);
		}
	}
	pthread_exit(EXIT_SUCCESS);

}

/*
 * Function Name: sendMultiframe
 * Input:
 * Outup:
 */
static void sendMultiframeManager( void )
{
	static tU16 sent_byte = 0;
	int i;


	struct can_frame frame;
	frame.can_id = CAN_ID;


	if( getWaitFc() )
		return;

	switch( mf_status )
	{
	case send_first_frame:
		if( !getWaitFc() )
		{
			frame.data[0] = MULTIFRAME_IDENTIFIER;
			frame.data[1] = mf_to_send_byte;
			sent_byte = 0;
			for(i=0; i < 6; i++)
			{
				frame.data[2+i] = *(buffer+i);
				sent_byte ++;
			}
			frame.can_dlc = 8;
			frame_counter = 0x20;
			setMfInProgress(TRUE);
			setWaitFc(TRUE);
			canWrite( frame );
		}
		break;

	case send_other_frames:
		if( (sent_packet < packet_at_once) && (sent_byte < mf_to_send_byte) )
		{
			if(frame_counter >= 0x2f)
				frame_counter = 0x20;
			else
				frame_counter ++;

			frame.can_dlc = (mf_to_send_byte - sent_byte >= 8) ? 8 : (mf_to_send_byte - sent_byte + 1);
			frame.data[0] = frame_counter;
			for(i=0; i < 7 && sent_byte < mf_to_send_byte; i++)
			{
				frame.data[i+1] = *(buffer + sent_byte);
				sent_byte ++;
			}
			canWrite( frame );
		}
		else
			if( !(sent_packet < packet_at_once) )
			{
				setWaitFc( TRUE );
			}
			else
				if( !(sent_byte < mf_to_send_byte) )
				{
					setWaitAck( TRUE );
				}
		break;
	}
}


/*
 * Function Name: sendMultiframe
 * Input:
 * Outup:
 */
static void sendMultiframeInit( const tU16 to_send_byte )
{
	mf_sent_byte = 0;
	mf_to_send_byte = to_send_byte;
	mf_status = send_first_frame;
	setWaitFc( FALSE );
	sendMultiframeManager();
}

/*
 * Function Name: resetEcu
 * Input:
 * Outup:
 */
static void resetEcu( void )
{
	struct can_frame frame;
	frame.can_id = 0x18EF8001 | CAN_EFF_FLAG;
	frame.can_dlc = 8;
	frame.data[0] = 0x46;
	frame.data[1] = 0x75;
	frame.data[2] = 0x4D;
	frame.data[3] = 0x34;
	frame.data[4] = 0x6E;
	frame.data[5] = 0x3C;
	frame.data[6] = 0x23;
	frame.data[7] = 0x4A;
	canWrite( frame );
}

/*
 * Function Name: startDiagnosticSession
 * Input:
 * Outup:
 */
static void startDiagnosticSession( void )
{
	struct can_frame frame;
	frame.can_id = CAN_ID;
	frame.can_dlc = 3;
	frame.data[0] = 0x02;
	frame.data[1] = START_DIAGNOSTIC_SESSION_CMD;
	frame.data[2] = 0x85;
	last_command_sent = START_DIAGNOSTIC_SESSION_CMD;
	canWrite( frame );
}

/*
 * Function Name: eraseFlash
 * Input:
 * Outup:
 */
static void eraseFlash( void )
{
	tU8 size = 0;

	buffer[size] = (tU8) ( ( ERASE_FLASH_CMD & 0xFF00 ) >> 8);
	size++;
	buffer[size] = (tU8) ( ( ERASE_FLASH_CMD & 0x00FF ) >> 0);
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_START & 0x00FF0000 ) >> 16);
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_START & 0x0000FF00 ) >> 8 );
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_START & 0x000000FF ) >> 0 );
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_END & 0x00FF0000 ) >> 16);
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_END & 0x0000FF00 ) >> 8 );
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_END & 0x000000FF ) >> 0 );
	size++;
	last_command_sent = (tU8) ( ( ERASE_FLASH_CMD & 0xFF00 ) >> 8);

	sendMultiframeInit( size );
}

/*
 * Function Name: eraseFlashWait
 * Input:
 * Outup:
 */
static void eraseFlashWait( void )
{
	struct can_frame frame;
	frame.can_id = CAN_ID;
	frame.can_dlc = 3;
	frame.data[0] = 0x02;
	frame.data[1] = (tU8) ( ( ERASE_FINISHED_FLASH_CMD & 0xFF00 ) >> 8);
	frame.data[2] = (tU8) ( ( ERASE_FINISHED_FLASH_CMD & 0x00FF ) >> 0);
	last_command_sent = (tU8) ( ( ERASE_FINISHED_FLASH_CMD & 0xFF00 ) >> 8);
	canWrite( frame );
}

/*
 * Function Name: eraseFlash
 * Input:
 * Outup:
 */
static void prepareDataTransfer( void )
{
	tU8 size = 0;

	buffer[size] = PREPARE_DATA_TRANSFER_CMD;
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_START & 0x00FF0000 ) >> 16);
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_START & 0x0000FF00 ) >> 8 );
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_START & 0x000000FF ) >> 0 );
	size++;
	buffer[size] = 0x00;
	size++;
	buffer[size] = (tU8) ( ( (ERASE_ADDR_START - ERASE_ADDR_END) & 0x00FF0000 ) >> 16);
	size++;
	buffer[size] = (tU8) ( ( (ERASE_ADDR_START - ERASE_ADDR_END) & 0x0000FF00 ) >> 8 );
	size++;
	buffer[size] = (tU8) ( ( (ERASE_ADDR_START - ERASE_ADDR_END) & 0x000000FF ) >> 0 );
	size++;
	last_command_sent = PREPARE_DATA_TRANSFER_CMD;

	sendMultiframeInit( size );
}

/*
 * Function Name: dataTransfer
 * Input:
 * Outup:
 */
//#define STEPS	20
static void dataTransfer( void )
{
	FILE* fp;
	int readed = 0;
	int i;

	fp = fopen(file_name, "rb");
	if (fp == NULL){
		ko();
		LOOP = FALSE;
	}

	buffer[0] = DATA_TRANSFER_CMD;

	fseek( fp, readed_byte, SEEK_SET);
	readed = fread( &buffer[1], 1, mf_max_data_to_send, fp);

	for(i=1; i<=readed; i++)
		ck = ck - 0xff + buffer[i];

	readed_byte += readed;
	fclose(fp);

	last_command_sent = DATA_TRANSFER_CMD;
	sendMultiframeInit( readed + 1 );
}

/*
 * Function Name: startDiagnosticSession
 * Input:
 * Outup:
 */
static void dataTransferFinish( void )
{
	struct can_frame frame;
	frame.can_id = CAN_ID;
	frame.can_dlc = 2;
	frame.data[0] = 0x01;
	frame.data[1] = DATA_TRANSFER_FINISH_CMD;
	last_command_sent = DATA_TRANSFER_FINISH_CMD;
	canWrite( frame );
}

/*
 * Function Name: compareChecksum
 * Input:
 * Outup:
 */
static void compareChecksum( void )
{
	tU8 size = 0;

	buffer[size] = (tU8) ( ( COMPARE_CHECKSUM_CMD & 0xFF00 ) >> 8);
	size++;
	buffer[size] = (tU8) ( ( COMPARE_CHECKSUM_CMD & 0x00FF ) >> 0);
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_START & 0x00FF0000 ) >> 16);
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_START & 0x0000FF00 ) >> 8 );
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_START & 0x000000FF ) >> 0 );
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_END & 0x00FF0000 ) >> 16);
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_END & 0x0000FF00 ) >> 8 );
	size++;
	buffer[size] = (tU8) ( ( ERASE_ADDR_END & 0x000000FF ) >> 0 );
	size++;
	buffer[size] = (tU8) ( ( ck & 0x0000FF00 ) >> 8 );
	size++;
	buffer[size] = (tU8) ( ( ck & 0x000000FF ) >> 0 );
	size++;

	last_command_sent = (tU8) ( ( COMPARE_CHECKSUM_CMD & 0xFF00 ) >> 8);
	sendMultiframeInit( size );
}

/*
 * Function Name: startDiagnosticSession
 * Input:
 * Outup:
 */
static void compareChecksumFinish( void )
{
	struct can_frame frame;
	frame.can_id = CAN_ID;
	frame.can_dlc = 3;
	frame.data[0] = 0x02;
	frame.data[1] = (tU8) ( ( COMPARE_CHECKSUM_RESULT_CMD & 0xFF00 ) >> 8);
	frame.data[2] = (tU8) ( ( COMPARE_CHECKSUM_RESULT_CMD & 0x00FF ) >> 0);
	last_command_sent = (tU8) ( ( COMPARE_CHECKSUM_RESULT_CMD & 0xFF00 ) >> 8);
	canWrite( frame );
}

/*
 * Function Name: getWaitFc
 * Input:
 * Outup:
 */
tU32  getReadedByte( void )
{
	return readed_byte;
}

/*
 * Function Name: getWaitFc
 * Input:
 * Outup:
 */
tU8  getReadedBytePercentage( void )
{
	return ((readed_byte * 100) / file_size );
}


/*
 * Function Name: getWaitFc
 * Input:
 * Outup:
 */
downloader_sm  getDownloaderState( void )
{
	return state;
}

/*
 * Function Name: getWaitFc
 * Input:
 * Outup:
 */
void  setDownloaderStateToInit( void )
{
	/* In poche parole if( non_ho_un_cazzo_da_fare) */
	if( wait_new_cmd  == state ||
			upgrade_complete_successfully == state ||
			error_state == state
	)
		state = init;
}

/*
 * Function Name: getWaitFc
 * Input:
 * Outup:
 */
int  setFileName(  char *name )
{
	FILE *fp = fopen( name,"r");
	if(fp == NULL){
		fclose(fp);
		return 1;
	}else{
		strcpy( file_name, name);
		fseek(fp, 0, SEEK_END);
		file_size = (tU32)ftell(fp);
		fclose(fp);
		debug("File name: %s File size: %ld\n", file_name, file_size);
		return 0;
	}
}


/*
 * Function Name: stateMachineManager
 * Input:
 * Outup:
 */
void stateMachineManager(void)
{
	pthread_t can_thread;
	//se multiframe in corso, gestisco quello
	if( getMfInProgress())
		sendMultiframeManager();

	switch( state )
	{

	case wait_new_cmd:
		break;

	case init:
		debug0("state: init\n");
		readed_byte = 0;
		retry = 0;
		ck = (tU16) (0xFF * ( (tU32)(ERASE_ADDR_END - ERASE_ADDR_START ) )) ;
		setWaitAck(FALSE);
		setWaitFc(FALSE);
		setMfInProgress(FALSE);
		pthread_mutex_init( &mutex_wait_ack, NULL );
		pthread_mutex_init( &mutex_wait_fc, NULL );
		pthread_mutex_init( &mutex_mf_in_progress, NULL );
		pthread_mutex_init( &mutex_timeout_expired, NULL );
		pthread_create( &can_thread, NULL, ackReader, (void *)0 );
		state++;
		break;

	case reset_ecu:
		//02 10 85
		debug0("state: reset_ecu\n");
		resetEcu();
		state++;
		retry = 0;
		break;

	case start_diagnostic_session:
		//02 10 85
		if(!getWaitAck()){
			debug0("state: start_diagnostic_session\n");
			retry ++;
			startDiagnosticSession();
			setWaitAck(TRUE);
			setTimeOutMs( 400 );
		}

		if( getTimeOutExpired() )
		{
			debug0("start_diagnostic_session setTimeOutMs %d \n", retry );
			if( retry < 4 ){
				setWaitAck(FALSE);
				retry ++;
			}
			else
				state = error_state;
		}
		break;

	case start_diagnostic_session_2:
		//02 10 85
		if(!getWaitAck()){
			debug0("state: start_diagnostic_session\n");
			retry ++;
			startDiagnosticSession();
			setWaitAck(TRUE);
			setTimeOutMs( 400 );
		}

		if( getTimeOutExpired() )
			state = error_state;
		break;

	case erase_flash:
		//10 08 31 02 SA SA SA EA EA EA
		if(!getWaitAck()){
			debug0("state: erase_flash\n");
			retry ++;
			eraseFlash();
			setWaitAck(TRUE);
			setTimeOutMs( 1000 );
		}
		if( getTimeOutExpired() )
			state = error_state;
		break;

	case erase_flash_wait:
		//02 33 02
		if(!getWaitAck()){
			debug0("state: erase_flash_wait\n");

			setWaitAck(TRUE);
			eraseFlashWait();
			setTimeOutMs( 7000 );
		}

		if( getTimeOutExpired() )
			state = error_state;
		break;

	case prepare_data_transfer:
		//10 08 34 SA SA SA 00 EA EA EA 00
		if(!getWaitAck()){
			debug0("state: prepare_data_transfer\n");

			setWaitAck(TRUE);
			prepareDataTransfer();
			setTimeOutMs( 1000 );
		}

		if( getTimeOutExpired() )
			state = error_state;
		break;

	case data_transfer:
		//10 XX 36 DA DA DA DA DA ..........
		if(!getWaitAck()){
			//debug("state: data_transfer\n");
			setWaitAck(TRUE);
			dataTransfer();
			setTimeOutMs( 1000 );
		}

		if( getTimeOutExpired() )
			state = error_state;
		break;

	case data_transfer_finish:
		//01 37
		if(!getWaitAck()){
			debug0("state: data_transfer_finish\n");

			setWaitAck(TRUE);
			dataTransferFinish();
			setTimeOutMs( 1000 );
		}

		if( getTimeOutExpired() )
			state = error_state;
		break;

	case compare_checksum:
		//10 XX 31 01 SA SA SA EA EA EA CS CS
		if(!getWaitAck()){
			debug0("state: compare_checksum\n");

			setWaitAck(TRUE);
			compareChecksum();
			setTimeOutMs( 1000 );
		}

		if( getTimeOutExpired() )
			state = error_state;
		break;

	case compare_checksum_finish:
		//02 33 01
		if(!getWaitAck()){
			debug0("state: compare_checksum_finish\n");
			setWaitAck(TRUE);
			compareChecksumFinish();
			setTimeOutMs( 1000 );
		}

		if( getTimeOutExpired() )
			state = error_state;
		break;

	case upgrade_complete_successfully:
		break;

	case error_state:
	default:
		//gestire errore!
		//debug0("state: error\n");
		break;

	}
}


