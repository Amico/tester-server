/*
 * can.h
 *
 *  Created on: 29/gen/2015
 *      Author: Luigi Scagnet - Egicon s.r.l.
 */

#ifndef CANBUS_H_
#define CANBUS_H_

#include <linux/can.h>

#define SOL_CAN_RAW (SOL_CAN_BASE + CAN_RAW)
enum {
	CAN_RAW_FILTER = 1,	/* set 0 .. n can_filter(s)          */
	CAN_RAW_ERR_FILTER,	/* set filter for error frames       */
	CAN_RAW_LOOPBACK,	/* local loopback (default:on)       */
	CAN_RAW_RECV_OWN_MSGS,	/* receive my own msgs (default:off) */
	CAN_RAW_FD_FRAMES,	/* allow CAN FD frames (default:off) */
	CAN_RAW_JOIN_FILTERS,	/* all filters must match to trigger */
};


//global data var
extern tU8 loop;

int canInit(char *interface);
int canSendMultiFrameAck(void);
int canWrite( struct can_frame frame );
void canRead( struct can_frame *frame );


void *canListenerr(void *parent_PID);


void can0_init_thread(void);
void can0_send0(void);
void can0_send1(void);
#endif /* CANBUS_H_ */
