/*
 * define.h
 *
 *
 * define.h
 *
 *  Created on: 29/gen/2015
 *      Author: Luigi Scagnet - Egicon s.r.l.
 */

#ifndef DEFINE_H_
#define DEFINE_H_

/* regole software concentratore
 * Funzioni: nome modulo + nome funzione in lowerCamelCase es. canInit()
 * Variabili locali: notazione underscores
 *
 *
 */
#include <stdio.h>
#define MAJOR_SW_RELEASE	0
#define MINOR_SW_RELEASE	1

#define STD_STR_SIZE		(255)
#ifndef TRUE
#define TRUE  				(1==1)
#endif

#ifndef FALSE
#define FALSE 				(1==0)
#endif

#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"


/*
 * MACRO per la stampa dei messaggi di debug.
 * Per abilitare il debug definire la macro DEBUG a inizio main.
 */
#ifdef DEBUG
	#define _DEBUG	1
	#define _DEBUG0	1
#else
	#define _DEBUG	0
#endif

#ifdef DEBUG0
	#define _DEBUG0	1
#else
	#define _DEBUG0	0
#endif

#define debug_cond(cond, fmt, args...)				\
	do {											\
		if (cond) {									\
			printf( "%s[DEBUG]:%s %sfunction: %s %s-->\t", KBLU, RESET, KCYN,__func__, RESET );	\
			printf(fmt, ##args);					\
		}											\
	} while (0)

#define debug(fmt, args...)			\
	debug_cond(_DEBUG, fmt, ##args)

#define debug0(fmt, args...)			\
	debug_cond(_DEBUG0, fmt, ##args)

#define info(fmt, args...)							\
		do {										\
			fprintf( stdout, "%s", KWHT );			\
			fprintf( stdout, fmt, ##args);			\
			fprintf( stdout, "%s ", RESET );			\
		} while (0)

#define ok()											\
		do {											\
			fprintf( stdout, "%sOk%s\n", KGRN, RESET );	\
		} while (0)

#define ko()											\
		do {											\
			fprintf( stdout, "%sKo%s\n", KRED, RESET );	\
		} while (0)


#define error(fmt, args...)			\
		do {											\
			fprintf( stderr, "%s[ERROR]:%s \t", KRED, RESET );	\
			fprintf( stderr, fmt, ##args);					\
		} while (0)

#define data(fmt, args...)										\
		do {													\
			fprintf( stdout, "%s[DATA]:%s --> ", KGRN, RESET );	\
			fprintf( stdout, fmt, ##args);						\
			fprintf( stdout, "\n");								\
		} while (0)

#define LOOP			system_flags.bit0

struct tU8flags
{
	unsigned char bit0 : 1;
	unsigned char bit1 : 1;
	unsigned char bit2 : 1;
	unsigned char bit3 : 1;
	unsigned char bit4 : 1;
	unsigned char bit5 : 1;
	unsigned char bit6 : 1;
	unsigned char bit7 : 1;
};

extern struct tU8flags system_flags;

typedef	unsigned char			tU8;
typedef	unsigned short int		tU16;
typedef	unsigned long int		tU32;
typedef	unsigned long long int	tU64;
typedef	signed char				tS8;
typedef	signed short int		tS16;
typedef	signed long int			tS32;
typedef	signed long long int	tS64;

typedef unsigned char 			boolean;
//typedef unsigned char 			bool;

#endif /* DEFINE_H_ */
