/*
 * hilevel_can.h
 *
 *  Created on: 15/giu/2015
 *      Author: devel
 */

#ifndef HILEVEL_CAN_H_
#define HILEVEL_CAN_H_
#include <define.h>

#define ROWS 		0x4000
#define COLS 		0x10

#define CAN_ID	0x5AC
#define ERASE_ADDR_START 	0x00008100
#define ERASE_ADDR_END  	0x00040000

#define MULTIFRAME_IDENTIFIER			0x10

#define START_DIAGNOSTIC_SESSION_CMD	0x10
#define READ_ECU_IDENTIFICATION_CMD		0x1A
#define ERASE_FLASH_CMD					0x3102
#define ERASE_FINISHED_FLASH_CMD		0x3302
#define PREPARE_DATA_TRANSFER_CMD		0x34
#define DATA_TRANSFER_CMD				0x36
#define DATA_TRANSFER_FINISH_CMD		0x37
#define COMPARE_CHECKSUM_CMD			0x3101
#define COMPARE_CHECKSUM_RESULT_CMD		0x3301



 /*
  *
  */
typedef enum {
	wait_new_cmd = 0,
	init,
	reset_ecu,
	start_diagnostic_session,
	start_diagnostic_session_2,
	erase_flash,
	erase_flash_wait,
	prepare_data_transfer,
	data_transfer,
	data_transfer_finish,
	compare_checksum,
	compare_checksum_finish,
	upgrade_complete_successfully,
	error_state,
} downloader_sm;


void stateMachineManager(void);
tU32  getReadedByte( void );
tU8  getReadedBytePercentage( void );
downloader_sm  getDownloaderState( void );
void  setDownloaderStateToInit( void );
int  setFileName( char *name );

#endif /* HILEVEL_CAN_H_ */
